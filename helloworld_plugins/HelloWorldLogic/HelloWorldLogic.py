#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# just fire an example custom event on load

from FalkPluginBase import FalkPluginBase

class HelloWorldLogic(FalkPluginBase):

   def __init__(self):
      FalkPluginBase.__init__(self)

      # common vars #
      #self.threaded = True # uncomment to use threading

      #self.wantsCfg = True # uncomment to ask for specific configurations from a config file

      # events to define or connect to this plugin, in this case, just define the load event
      # you can specify any custom event event you want in this dictionary
      # and fire them in this or any other plugin using self.fireEv("on_custom_event")
      self.events   = { 
                        "on_startup"           : [],
                        "on_askhelloworld"     : []
                      }

      # custom configurations to ask to the config manager
      #self.fbConfig = {
      #                 "config_name" : "default_value",
      #                }
      
      self.name     = "HelloWorldLogic"         # plugin name used as section name in the config file
      #self.cName    = "helloworld.ini"         # config file name located into the main configuration folder

   # custom method, ask the other plugin to print hello world
   def hello_event(self, event): 
       self.fireEv("on_askhelloworld")

   # default method used to connect the events to the 
   def connect_events(self):
      self.events["on_startup"].append(self.hello_event)
 
