#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# receive the ask event and print hello world

from FalkPluginBase import FalkPluginBase

class HelloWorldPrint(FalkPluginBase):

   def __init__(self):
      FalkPluginBase.__init__(self)

      # common vars #
      #self.threaded = True # uncomment to use threading

      #self.wantsCfg = True # uncomment to ask for specific configurations from a config file

      # events to define or connect to this plugin, in this case, just define the load event
      self.events   = { 
                        "on_askhelloworld"     : [],
                      }

      # custom configurations to ask to the config manager
      #self.fbConfig = {
      #                 "config_name" : "default_value",
      #                }
      
      self.name     = "HelloWorldLogic"         # plugin name used as section name in the config file
      #self.cName    = "helloworldlogic.ini"         # config file name located into the main configuration folder

   # custom method, ask the other plugin to print hello world
   def hello_world(self, event): 
       print("Hello World!")

   # default method used to connect the events to the 
   def connect_events(self):
      self.events["on_askhelloworld"].append(self.hello_world)
 
