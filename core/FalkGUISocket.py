#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# abstract gui socket wrapper to standardize calls to an interface element like a vbox or hbox

class FalkGUISocket:

    def __init__(self, socket):
        self.socket = socket    # must be an instanced vbox or another cointainer element regardless of the interface


    # add an element to the socket #
    def add_element(self, element, position, data=None):
        pass


    # set or add spacing to the socket #
    def set_spacing(self, spacing, data=None):
        pass

