#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Simple event system built in python

from sys import exc_info
from traceback import print_tb

class FalkEv:
   
    def __init__(self, verbose):
        self.verbose = verbose

        self.events  = {
                         "on_startup"        : [],
                         "on_loadplugin"     : [],
                         "on_unloadplugin"   : [],
                         "on_pluginloaded"   : [],
                         "on_pluginunloaded" : [],
                         "on_eventerror"     : [],
                         "on_reload_cfg"     : [],
                         "on_reinit_plugins" : [],
                         "on_reload_all"     : []
                        }
                        
   
    ### Event Interface ###
   
    # fires events and launch every function #
    def fire_event(self, event_name, *args, **kwargs):  
        if self.verbose:
            print(event_name)

        if event_name in self.events:
            for f in self.events[event_name]:
                try:
                    f(event_name, *args, **kwargs)

                except Exception as e:
                    exc_type, exc_obj, exc_tb = exc_info() 

                    # don't loop if you catch an exception during handling another one #
                    if event_name != "on_eventerror":
                        self.fire_event("on_eventerror", event_name, f, e, exc_tb)

                    print("FalkEv ERROR during event {} while calling {}.{}:\n{}".format(event_name,
                                                                                            f.__self__.name,
                                                                                            f.__name__,
                                                                                            e))
                    print_tb(exc_tb)

        elif self.verbose:
            print("WARNING no such event: {}".format(event_name))


    # add functions in the event dictionary #
    def add_event_func(self, event_dict):
        for event_name in event_dict:
            if event_name in self.events:
               
                if isinstance(event_dict[event_name], list):
                    for func_name in event_dict[event_name]:
                        self.events[event_name].append(func_name)
                     
                else:
                    self.events[event_name].append(event_dict[event_name])

            else:
                if isinstance(event_dict[event_name], list):
                    self.events[event_name] = event_dict[event_name]
            
                else:
                    self.events[event_name] = [event_dict[event_name]]

     
    # remove a function from the dictionary #
    def rem_event_func(self, event_dict):
        for event_name in event_dict:
            if event_name in self.events: 
                if isinstance(event_dict[event_name], list):
                    for func_name in event_dict[event_name]:
                        if func_name in self.events[event_name]:
                            self.events[event_name].remove(func_name)
                     
                elif event_dict[event_name] in self.events[event_name]:
                    self.events[event_name].remove(event_dict[event_name])


   
    # debug function prints every event #
    def print_events(self):
        for key in self.events:
            print(key, str(self.events[key]))
         
   
