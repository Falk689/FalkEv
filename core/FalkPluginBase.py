#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# base plugin, the others will inherit from this #

class FalkPluginBase:
   
    def __init__(self):
        # common vars #
        self.priority = 0                   # can be 0|1|2. 0 = high, 1 = medium, 2 = low
        self.type     = "logic"             # this can be logic or interface
        self.fireEv   = None                # in this var is stored the function that fires events, you shouldn't edit it directly since it happens in FalkPlugins
        self.threaded = False               # is the plugin threaded?
        self.wantsCfg = False               # does the plugin want configs?
        self.thread   = None                # thread interface

        self.fbConfig = {}                  # fallback configs, used to store default values when there's no sections
        self.configs  = {}                  # dictionary containing requested setting storage objects
        self.events   = {}                  # events and relative reactions of this plugin
      
        self.name     = "FalkPluginBase"    # plugin name used as main section name in the config file
        self.cName    = None                # config file name, can be None or name.ini for FalkCfg.configPath/name.ini
        self.groups   = ["Main", "Args"]    # config groups, secondary sections in the config file the plugin will read from


    ### DEFAULT METHODS ###
   
    # connect local events with callbacks, deprecated #
    def connect_events(self):
        #Example: self.events["event_name"].append(self.function)
        pass

    # read self.configs and returns the proper value for a specified key #
    def read_config(self, cName):
        if self.name in self.configs:
            if cName in self.configs[self.name].configs:
                return self.configs[self.name].configs[cName]

        for group in self.groups:
            if group in self.configs and cName in self.configs[group].configs:
                return self.configs[group].configs[cName]
            
        return None


    # edit a setting, doesn't write in the file but affects current session #
    def write_config(self, cName, value):
        if self.name in self.configs and cName in self.configs[self.name].configs:
            self.configs[self.name].configs[cName] = value

    
    # wrapper to easily reload configurations #
    def reload_config(self, main=False):
        self.fireEv("on_reload_cfg", self.name, self.groups, self.fbConfig, main)


    # wrapper to easily reload configurations and plugins #
    def reload_all(self, main=True): 
        self.fireEv("on_reload_all", self.name, self.groups, self.fbConfig, main)


    # called on unload to properly remove the plugin #
    def kill(self):
        del self
