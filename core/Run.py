#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# FalkEv main executable, you should edit and run this script to start a program written with FalkEv

from argparse import ArgumentParser

from FalkPlugins import FalkPlugins

if __name__ == "__main__":
    # configuration file path
    configPath = (".config/helloworld_config", "C:/falkev/helloworld_config")


    # argument parser
    parser = ArgumentParser(description="FalkEv main executable default text... you should edit this.") 
    parser.add_argument("-v", action="store_true", default=False,
                       dest="verbose",
                       help="prints debug info")

    args = parser.parse_args() 


    # instance and startup the plugin system
    fp = FalkPlugins(configPath, args) 
    fp.startup()
