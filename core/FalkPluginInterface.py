#!/usr/bin/env python2
# -*- coding: UTF-8 -*-

# base GTK plugin 

from FalkPluginBase import FalkPluginBase

class FalkPluginInterface(FalkPluginBase):
   
    def __init__(self):
        FalkPluginBase.__init__(self)

        self.type     = "interface"           # this can be logic or interface
        self.name     = "FalkPluginInterface" # plugin name used as section in config file

        # default interface vars #
        self.isRoot   = False                 # is the plugin the base of an interface system?  
        self.root     = None                  # top element of the interface
        self.socket   = None                  # if this is a root element, this will become the main socket 
