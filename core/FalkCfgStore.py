#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Simple config container, used to centralize configuration storage and pass them safely to the plugins

class FalkCfgStore:

    def __init__(self):
        self.configs = {}

