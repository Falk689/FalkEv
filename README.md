# FalkEv

Simple event and plugin based framework built to provide common features to easy projects and scripts

## Example project
This example just prints "Hello World!" in a purposely convoluted way to show the main interaction mechanism between FalkEv plugins.
For a slightly more useful and in-depth example see https://gitlab.com/Falk689/pykodi

## Getting started

If you're on a unix-like system, the easiest way to test this example is to move the "helloworld_config" folder in "~/.config" and "helloworld_plugins" in your home folder.

In Windows (or in case you want to change the default path) you should edit "core/Run.py" to point to the location of "helloworld_config" folder then edit "helloworld_config/main.ini" to point the "helloworld_plugins" folder.

After the paths are specified correctly, to run the program just execute "core/Run.py" with python3

